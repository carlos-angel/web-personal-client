# Web personal client ![Status badge](https://img.shields.io/badge/status-in%20progress-yellow)

Web-site de una pagina web personal donde podrás compartir contenido con todo el mundo. 

## Instalación
1. Clona este proyecto.
2. Ve a la carpeta del proyecto `cd web-personal-client`
3. Correr el servidor `web-personal-server` que puedes clonar [Aquí](https://gitlab.com/Charly_Angel/web-personal-server)
4. Instala las dependencias ejecutando el comando `yarn install`
5. Corre el ambiente local ejecutando el comando `yarn start`
6. Construye el proyecto para el ambiente en producción ejecutando el comando `yarn build`

## License
The MIT License (MIT)
