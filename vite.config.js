import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import svgr from 'vite-plugin-svgr';
import { resolve } from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), svgr()],
  resolve: {
    alias: {
      api: resolve('src/api/'),
      assets: resolve('src/assets/'),
      components: resolve('src/components/'),
      config: resolve('src/config/'),
      content: resolve('src/content/'),
      context: resolve('src/context/'),
      hooks: resolve('src/hooks/'),
      pages: resolve('src/pages/'),
      layouts: resolve('src/layouts/'),
      providers: resolve('src/providers/'),
      routes: resolve('src/routes/'),
      scss: resolve('src/scss/'),
      utils: resolve('src/utils/'),
    },
  },
});
