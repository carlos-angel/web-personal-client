import React from 'react';
import AcademyLogo from 'assets/images/png/academy-logo.png';
import './PresentationCourse.scss';

export default function PresentationCourse() {
  return (
    <div className="presentation-course">
      <img src={AcademyLogo} alt="Cursos de Carlos Alberto Angel Angel" />
      <p>
        En nuestra academia, encontraras los mejores cursos online de desarrollo
        web en español. Únete a nosotros y empieza tu camino como un
        Desarrollador web o Desarrollador CMS. Sinceramente, estos cursos son el
        tipo de contenido que me hubiera gustado encontrar cunado comencé mi
        aprendizaje como Dessarrollador web.
      </p>
      <p> ¡Que esperas!</p>
      <p>¡¡Ve su contenido y aprovecha nuestras ofertas!! </p>
    </div>
  );
}
