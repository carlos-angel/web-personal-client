import React from 'react';
import { Card, Button } from 'antd';
import { coursesApi } from 'api/courses';
import { useImage } from 'hooks/useImage';

export default function Course({ course }) {
  const [image] = useImage(coursesApi.getImage(course.image));

  return (
    <a href={course.link} target="_blank" rel="noopener noreferrer">
      <Card
        cover={
          <img src={image} alt={course.title} style={{ height: '250px' }} />
        }
      >
        <Card.Meta title={course.title} />
        <Button>Ir al curso</Button>
      </Card>
    </a>
  );
}
