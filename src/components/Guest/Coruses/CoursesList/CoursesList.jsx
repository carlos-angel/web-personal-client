import React from 'react';
import { Row, Col } from 'antd';

import Course from './Course';

import './CoursesList.scss';

export default function CoursesList({ courses }) {
  return (
    <div className="courses-list">
      <Row>
        {courses.map((course) => {
          const { _id } = course;
          return (
            <Col md={8} key={_id} className="courses-list__course">
              <Course course={course} />
            </Col>
          );
        })}
      </Row>
    </div>
  );
}
