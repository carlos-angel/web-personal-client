import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Spin, notification } from 'antd';
import moment from 'moment';
import { getPostApi } from 'api/post';
import 'moment/locale/es';
import './PostInfo.scss';

export default function PostInfo({ url }) {
  const [postInfo, setPostInfo] = useState(null);

  useEffect(() => {
    getPostApi(url)
      .then((response) => (!response.error
        ? setPostInfo(response.data)
        : notification.warning({ message: response.message })))
      .catch((errorMessage) => notification.warning({ message: errorMessage }));
  }, [url]);

  if (!postInfo) {
    return <Spin tip="cargando" style={{ width: '100%', padding: '200px 0' }} />;
  }

  return (
    <>
      <Helmet>
        <title>{`${postInfo.title} | Carlos Alberto Angel Angel`}</title>
        <meta
          name="description"
          content={`${postInfo.title} | información de programación web`}
          data-react-helmet="true"
        />
      </Helmet>
      <div className="post-info">
        <h1 className="post-info__title">{postInfo.title}</h1>
        <div className="post-info__creation-date">{moment(postInfo.date).format('LL')}</div>
        <div
          className="post-info__description"
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: postInfo.description }}
        />
      </div>
    </>
  );
}
