import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Spin, List, notification } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import queryString from 'query-string';

import { getPublishPostsApi } from 'api/post';
import Pagination from 'components/Shared/Pagination';

import 'moment/locale/es';
import './PostsList.scss';

export default function PostsList({ location, history }) {
  const [posts, setPosts] = useState(null);
  const { page = 1 } = queryString.parse(location.search);

  useEffect(() => {
    getPublishPostsApi(page).then((response) => (!response.error
      ? setPosts(response.data)
      : notification.warning({ message: response.message })));
  }, [page]);

  if (!posts) {
    return <Spin tip="Cargando" style={{ width: '100%', padding: '20px 0' }} />;
  }

  return (
    <>
      <Helmet>
        <title>Blog de programación | Carlos Alberto Angel Angel</title>
        <meta
          name="description"
          content="Blog | Web sobre programación"
          data-react-helmet="true"
        />
      </Helmet>
      <div className="posts-list-web">
        <h1>Posts</h1>
        <List
          dataSource={posts.docs}
          renderItem={(post) => <Post post={post} />}
        />
        <Pagination
          location={location}
          history={history}
          total={posts.total}
          defaultCurrent={parseInt(posts.page, 10)}
          limit={posts.limit}
        />
      </div>
    </>
  );
}

function Post({ post }) {
  const day = moment(post.date).format('DD');
  const month = moment(post.date).format('MMMM');

  return (
    <List.Item className="post">
      <div className="post__date">
        <span>{day}</span>
        <span>{month}</span>
      </div>
      <List.Item.Meta
        title={<Link to={`blog/${post.slug}`}>{post.title}</Link>}
      />
    </List.Item>
  );
}
