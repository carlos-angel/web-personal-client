import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button } from 'antd';
import { Link } from 'react-router-dom';
import CardCourse from './CardCourse';
import './HomeCourses.scss';

export default function HomeCourses({ courses }) {
  return !courses ? null : (
    <Row className="home-courses">
      <Col lg={24} className="home-courses__title">
        <h2>Aprende y mejora tus habilidades</h2>
      </Col>
      <Col lg={4} />
      <Col lg={16}>
        <Row className="row-courses">
          {courses.map((course) => {
            const { _id } = course;
            return (
              <Col md={6} key={_id}>
                <CardCourse {...course} />
              </Col>
            );
          })}
        </Row>
      </Col>
      <Col lg={4} />
      <Col lg={24} className="home-courses__more">
        <Link to="/courses">
          <Button> ver más</Button>
        </Link>
      </Col>
    </Row>
  );
}

HomeCourses.propTypes = {
  courses: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      link: PropTypes.string.isRequired,
    }),
  ).isRequired,
};
