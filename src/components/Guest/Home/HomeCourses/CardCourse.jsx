import React from 'react';
import PropTypes from 'prop-types';
import { Card, Button } from 'antd';
import { coursesApi } from 'api/courses';
import { useImage } from 'hooks/useImage';

function CardCourse({ image, title, link }) {
  const [url] = useImage(coursesApi.getImage(image));

  const { Meta } = Card;
  return (
    <a href={link} target="_blank" rel="noopener noreferrer">
      <Card
        className="home-courses__card"
        cover={<img src={url} alt={title} style={{ height: '125px' }} />}
        actions={[<Button>Ingresar al curso</Button>]}
      >
        <Meta title={title} />
      </Card>
    </a>
  );
}

CardCourse.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
};

export default CardCourse;
