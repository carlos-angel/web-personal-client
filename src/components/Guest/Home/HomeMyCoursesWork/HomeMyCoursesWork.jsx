/* eslint-disable max-len */
import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';
import CardInformation from './CardInformation';
import './HomeMyCoursesWork.scss';

export default function HomeMyCoursesWork({ cardsCourses }) {
  return (
    <Row className="home-my-courses-work">
      <Col lg={24} className="home-my-courses-work__title">
        <h2>¿Cómo funcionan mis cursos?</h2>
        <h3>Cada curso cuenta con contenido bajo la web de Udemy, activa las 24 horas al día de los 365 días del año</h3>
      </Col>
      <Col lg={4} />
      <Col lg={16}>
        <Row className="row-cards">
          {cardsCourses.map(({
            _id,
            icon,
            title,
            description,
          }) => (
            <Col md={8} key={_id}>
              <CardInformation icon={icon} title={title} description={description} />
            </Col>
          ))}
        </Row>
      </Col>
      <Col lg={4} />
    </Row>
  );
}

HomeMyCoursesWork.propTypes = {
  cardsCourses: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      icon: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    }),
  ).isRequired,
};
