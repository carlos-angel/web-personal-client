import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'antd';
import * as AntdIcons from '@ant-design/icons';

function CardInformation({ icon, title, description }) {
  const { Meta } = Card;
  const AntdIcon = AntdIcons[icon];
  return (
    <Card className="home-my-courses-work__card">
      <AntdIcon />
      <Meta title={title} description={description} />
    </Card>
  );
}

CardInformation.propTypes = {
  icon: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default CardInformation;
