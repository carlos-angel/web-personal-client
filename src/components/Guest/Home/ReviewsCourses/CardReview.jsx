import React from 'react';
import PropTypes from 'prop-types';
import { Card, Avatar } from 'antd';
import { apiReview } from 'api/review.api';
import { useImage } from 'hooks/useImage';
import NotFound from 'assets/images/png/notFound.png';

function CardReview({
  name, jobTitle, image, review,
}) {
  const [avatar] = useImage(apiReview.getImage(image));

  return (
    <Card className="reviews-courses__card">
      <p>{review}</p>
      <Card.Meta
        avatar={<Avatar src={image ? avatar : NotFound} />}
        title={name}
        description={jobTitle}
      />
    </Card>
  );
}

CardReview.propTypes = {
  name: PropTypes.string.isRequired,
  jobTitle: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  review: PropTypes.string.isRequired,
};

export default CardReview;
