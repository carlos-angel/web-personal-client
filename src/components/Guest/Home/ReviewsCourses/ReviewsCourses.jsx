import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';
import CardReview from './CardReview';
import './ReviewsCourses.scss';

export default function ReviewsCourses({ reviews }) {
  return (
    <Row className="reviews-courses">
      <Col lg={4} />
      <Col lg={16} className="reviews-courses__title">
        <h2>
          Forma parte de los +35 mil estudiantes que están aprendiendo con mis
          cursos
        </h2>
      </Col>
      <Col lg={4} />

      <Row>
        <Col lg={4} />
        <Col lg={16}>
          <Row className="row-cards">
            {reviews.map((review) => {
              const { _id } = review;
              return (
                <Col md={8} key={_id}>
                  <CardReview {...review} />
                </Col>
              );
            })}
          </Row>
        </Col>
        <Col lg={4} />
      </Row>
    </Row>
  );
}

ReviewsCourses.propTypes = {
  reviews: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      jobTitle: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired,
      review: PropTypes.string.isRequired,
    }),
  ).isRequired,
};
