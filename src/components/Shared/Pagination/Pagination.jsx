import React from 'react';
import { Pagination as PaginationAntd } from 'antd';

import './Pagination.scss';

export default function Pagination({
  location, history, total, limit, page,
}) {
  const onChangePage = (newPage) => {
    history.push(`${location.pathname}?page=${newPage}`);
  };

  return (
    <PaginationAntd
      defaultCurrent={page}
      total={total}
      pageSize={limit}
      onChange={(newPage) => onChangePage(newPage)}
      className="pagination"
    />
  );
}
