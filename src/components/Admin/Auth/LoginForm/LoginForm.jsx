import React, { useState } from 'react';
import {
  Form, Input, Button, notification,
} from 'antd';
import { UserOutlined, KeyOutlined } from '@ant-design/icons';
import { signInApi } from 'api/user';
import { ACCESS_TOKEN, REFRESH_TOKEN } from 'utils/constans';

import './LoginForm.scss';

export default function LoginForm() {
  const [loading, setLoading] = useState(false);

  return (
    <Form
      className="login-form"
      initialValues={{ email: '', password: '' }}
      onFinish={async (values) => {
        setLoading(true);
        const result = await signInApi(values);
        if (result.error) {
          notification.error({ message: result.message });
          setLoading(false);
        } else {
          const { accessToken, refreshToken } = result.data;
          localStorage.setItem(ACCESS_TOKEN, accessToken);
          localStorage.setItem(REFRESH_TOKEN, refreshToken);
          setLoading(false);
          window.location.href = '/admin'; // TODO: implementar un redirect con react router dom
        }
      }}
    >
      <Form.Item
        name="email"
        rules={[
          { required: true, message: 'email is required!' },
          { type: 'email', message: 'is invalid email!' },
        ]}
      >
        <Input prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="email" />
      </Form.Item>

      <Form.Item
        name="password"
        rules={[
          { required: true, message: 'password is required!' },
          { type: 'string', min: 8, message: 'the password must have more than 7 characters' },
        ]}
      >
        <Input prefix={<KeyOutlined style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="password" />
      </Form.Item>

      <Form.Item>
        <Button htmlType="submit" className="login-form__button" loading={loading}>
          Login
        </Button>
      </Form.Item>
    </Form>
  );
}
