import React, { useState } from 'react';
import {
  Form, Button, Input, Checkbox,
} from 'antd';
import { UserOutlined, KeyOutlined } from '@ant-design/icons';
import { createData } from 'utils/createData';
import { userApi } from 'api/user';

import './RegisterForm.scss';

function initialValues() {
  return {
    name: '',
    lastname: '',
    email: '',
    password: '',
    confirmPassword: '',
    privacyPolicy: false,
  };
}

export default function RegisterForm() {
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();

  return (
    <Form
      form={form}
      className="register-form"
      initialValues={initialValues()}
      onFinish={async (values) => {
        setLoading(true);
        const {
          name, lastname, email, password,
        } = values;
        const result = await createData(userApi.create, {
          name, lastname, email, password,
        });
        result && form.resetFields();
        setLoading(false);
      }}
    >
      <Form.Item name="name" rules={[{ required: true, message: 'name is required!' }]}>
        <Input prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Nombre" />
      </Form.Item>
      <Form.Item name="lastname" rules={[{ required: true, message: 'last name is required!' }]}>
        <Input prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Apellidos" />
      </Form.Item>
      <Form.Item
        name="email"
        rules={[
          { required: true, message: 'email is required!' },
          { type: 'email', message: 'is invalid email!' },
        ]}
      >
        <Input prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="email" />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          { required: true, message: 'password is required!' },
          { type: 'string', min: 8, message: 'the password must have more than 7 characters' },
        ]}
      >
        <Input prefix={<KeyOutlined style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Contraseña" />
      </Form.Item>
      <Form.Item
        name="confirmPassword"
        rules={[
          { required: true, message: 'please confirm you password!' },
          { type: 'string', min: 8, message: 'the password must have more than 7 characters' },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('The two passwords that you entered do not match!'));
            },
          }),
        ]}
      >
        <Input prefix={<KeyOutlined style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Confirmar contraseña" />
      </Form.Item>
      <Form.Item
        name="privacyPolicy"
        valuePropName="checked"
        rules={[
          {
            validator: (_, value) => (value ? Promise.resolve() : Promise.reject(new Error('you have to accept the privacy policy.'))),
          },
        ]}
      >
        <Checkbox>He leído y acepto la política de privacidad.</Checkbox>
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" className="register-form__button" loading={loading}>
          Registrar
        </Button>
      </Form.Item>
    </Form>
  );
}
