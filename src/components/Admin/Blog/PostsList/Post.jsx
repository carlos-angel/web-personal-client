import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { Switch, List, Button } from 'antd';
import { EyeOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';

import { postsApi } from 'api/post';
import { active } from 'utils/active';
import { deleteConfirm } from 'utils/deleteConfirm';
import { BlogContext } from 'pages/Admin/Blog/Blog';
import PostForm from 'components/Admin/Blog/Form';

export default function Post({ post }) {
  const {
    setIsVisibleModal, setTitleModal, setContentModal, setReloadList,
  } = useContext(BlogContext);
  const { _id } = post;

  const editPost = (postEdit) => {
    setIsVisibleModal(true);
    setTitleModal(`editar el post ${postEdit.title}`);
    setContentModal(<PostForm post={postEdit} />);
  };

  const showDeleteConfirm = (id, title) => deleteConfirm(`el post ${title}`, postsApi.delete(id), setReloadList);

  return (
    <List.Item
      actions={[
        <Switch defaultChecked={post.active} onChange={(e) => active(postsApi.active(_id), e)} />,
        <Link to={`/blog/${post.slug}`} target="_blank">
          <Button type="primary">
            <EyeOutlined />
          </Button>
        </Link>,
        <Button type="primary" onClick={() => editPost(post)}>
          <EditOutlined />
        </Button>,
        <Button type="danger" onClick={() => showDeleteConfirm(_id, post.title)}>
          <DeleteOutlined />
        </Button>,
      ]}
    >
      <List.Item.Meta title={post.title} />
    </List.Item>
  );
}
