import React from 'react';
import { List } from 'antd';
import './PostsList.scss';

export default function PostsList({ posts, render }) {
  return (
    <div className="posts-list">
      <List dataSource={posts} renderItem={render} />
    </div>
  );
}
