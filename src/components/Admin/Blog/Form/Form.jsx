import React, { useState, useEffect, useContext } from 'react';
import {
  Col, Form, Input, Button, DatePicker, notification,
} from 'antd';
import { FontSizeOutlined } from '@ant-design/icons';
import moment from 'moment';
import { Editor } from '@tinymce/tinymce-react';
import { postsApi } from 'api/post';
import { updateData } from 'utils/updateData';
import { createData } from 'utils/createData';
import { BlogContext } from 'pages/Admin/Blog/Blog';

import './Form.scss';

function initEditor() {
  return {
    height: 400,
    menubar: true,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount',
    ],
    toolbar:
      // eslint-disable-next-line no-multi-str
      'undo redo | formatselect | bold italic backcolor | \
    alignleft aligncenter alignright alignjustify | \
    bullist numlist outdent indent | removeformat | help',
  };
}

export default function PostForm({ isNew = false, post = { title: '', description: '' } }) {
  const [description, setDescription] = useState(post.description);

  const [form] = Form.useForm();
  const { setReloadList, setIsVisibleModal } = useContext(BlogContext);
  const { _id } = post;

  useEffect(() => {
    form.setFieldsValue({
      title: post.title || '',
      date: post.date ? moment(post.date) : null,
    });
  }, [post]);

  useEffect(() => {
    setDescription(post.description);
  }, [post.description]);

  const handleSubmit = async (values) => {
    if (!description) {
      notification.warning({ message: 'El Post no tiene contenido.' });
    } else {
      let result = false;
      if (isNew) result = await createData(postsApi.create, { ...values, description });
      else result = await updateData(postsApi.update(_id), { ...values, description });

      result && form.resetFields();
      setIsVisibleModal(!result);
      setReloadList(result);
    }
  };

  return (
    <div className="post-form">
      <Form form={form} onFinish={handleSubmit}>
        <Form.Item name="title" rules={[{ required: true, message: 'Please input a title!' }]}>
          <Input prefix={<FontSizeOutlined />} placeholder="Titulo del Post" />
        </Form.Item>
        <Col span={8}>
          <Form.Item
            name="date"
            rules={[{ type: 'date', required: true, message: 'Please selected right date!' }]}
          >
            <DatePicker
              style={{ width: '100%' }}
              format="DD/MM/YYYY HH:mm:ss"
              placeholder="Fecha de publicación"
            />
          </Form.Item>
        </Col>
        <Form.Item>
          <Editor
            value={description}
            init={initEditor()}
            onBlur={(e) => setDescription(e.target.getContent())}
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="btn-submit">
            {isNew ? 'Guardar' : 'Editar'}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
