import React, { useEffect } from 'react';
import {
  Form, Input, Select, Button, Row, Col, notification,
} from 'antd';
import { UserOutlined, MailOutlined, KeyOutlined } from '@ant-design/icons';
import Avatar from 'components/Admin/Avatar';
import { userApi } from 'api/user';
import { updateData } from 'utils/updateData';
import { createData } from 'utils/createData';
import './Form.scss';

function UserForm({
  isNew = false, data = {}, setIsVisibleModal, setReloadList,
}) {
  const [form] = Form.useForm();
  const { Option } = Select;
  const { _id } = data;

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue({
      name: data?.name || '',
      lastname: data?.lastname || '',
      email: data?.email || '',
      role: data?.role || undefined,
      password: '',
      confirmPassword: '',
    });
  }, [data, form]);

  const validatePassword = (user) => {
    if (user.password || user.confirmPassword) {
      if (user.password !== user.confirmPassword) {
        notification.error({ message: 'las passwords deben ser iguales' });
        return false;
      }
    } else {
      // eslint-disable-next-line no-param-reassign
      delete user.password;
    }

    // eslint-disable-next-line no-param-reassign
    delete user.confirmPassword;
    return true;
  };

  const onFinish = async (values) => {
    let result = false;
    const user = values;
    if (validatePassword(user)) {
      if (isNew) {
        result = await createData(userApi.create, user);
      } else {
        result = await updateData(userApi.update(_id), user);
      }
    }

    setIsVisibleModal(!result);
    setReloadList(result);
    result && form.resetFields();
  };

  return (
    <>
      {!isNew && (
        <Avatar
          getImageUrl={userApi.getImage(data.avatar)}
          updateImageUrl={userApi.updateImage(_id)}
          id={_id}
          setReloadList={setReloadList}
          circle
          isAvatar
        />
      )}
      <Form className="user-form" form={form} onFinish={onFinish}>
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item name="name" rules={[{ required: true, message: 'Please input a name!' }]}>
              <Input prefix={<UserOutlined />} placeholder="Nombre" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="lastname"
              rules={[{ required: true, message: 'Please input a last name!' }]}
            >
              <Input prefix={<UserOutlined />} placeholder="Apellidos" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={24}>
          <Col span={12}>
            <Form.Item name="email" rules={[{ required: true, message: 'Please input a email!' }]}>
              <Input prefix={<MailOutlined />} placeholder="correo electronico" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="role" rules={[{ required: true, message: 'Please selected a role!' }]}>
              <Select placeholder="Rol">
                <Option value="ROLE_ADMIN">Administrador</Option>
                <Option value="ROLE_USER">Usuario</Option>
                <Option value="ROLE_EDITOR">Editor</Option>
                <Option value="ROLE_REVIEW">Revisor</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              name="password"
              rules={[{ required: isNew, message: 'Please input a password!' }]}
            >
              <Input prefix={<KeyOutlined />} placeholder="contraseña" type="password" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="confirmPassword"
              rules={[
                {
                  required: isNew,
                  message: 'Please input a confirm password!',
                },
              ]}
            >
              <Input prefix={<KeyOutlined />} placeholder="confirmar contraseña" type="password" />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="btn-submit">
            {isNew ? 'Guardar' : 'Editar'}
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}

export default UserForm;
