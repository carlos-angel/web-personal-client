import React from 'react';
import { List } from 'antd';
import './UsersList.scss';

export default function UsersList({ users, renderItem }) {
  return (
    <List
      className="users"
      itemLayout="horizontal"
      dataSource={users}
      renderItem={renderItem}
    />
  );
}
