import React from 'react';
import { List, Avatar, Button } from 'antd';
import {
  EditOutlined,
  DeleteOutlined,
  StopOutlined,
  CheckOutlined,
} from '@ant-design/icons';
import { userApi } from 'api/user';
import { useImage } from 'hooks/useImage';
import { active } from 'utils/active';
import { deleteConfirm } from 'utils/deleteConfirm';
import NotAvatar from 'assets/images/png/no-avatar.png';

export default function User({ user, editUser, setReloadList }) {
  const [avatar] = useImage(userApi.getImage(user.avatar));
  const { _id } = user;

  const activeOrDesactiveUser = (isActive) => {
    const success = active(userApi.active(_id), isActive);
    success && setReloadList();
  };

  const showDeleteConfirm = (id, title) => deleteConfirm(`el usuario ${title}`, userApi.delete(id), setReloadList);

  const actionsItem = (activeUser) => {
    if (activeUser) {
      return [
        <Button type="primary" onClick={() => editUser(user)}>
          <EditOutlined />
        </Button>,
        <Button type="danger" onClick={() => activeOrDesactiveUser(false)}>
          <StopOutlined />
        </Button>,
        <Button
          type="danger"
          onClick={() => showDeleteConfirm(_id, user.email)}
        >
          <DeleteOutlined />
        </Button>,
      ];
    }
    return [
      <Button type="primary" onClick={() => activeOrDesactiveUser(true)}>
        <CheckOutlined />
      </Button>,
      <Button
        type="danger"
        onClick={() => showDeleteConfirm(_id, user.email)}
      >
        <DeleteOutlined />
      </Button>,
    ];
  };

  return (
    <List.Item actions={actionsItem(user.active)}>
      <List.Item.Meta
        avatar={<Avatar src={avatar || NotAvatar} />}
        title={`
            ${user.name ? user.name : '...'} 
            ${user.lastname ? user.lastname : '...'}`}
        description={user.email}
      />
    </List.Item>
  );
}
