import React, { useEffect } from 'react';
import { Form, Input, Button } from 'antd';
import { FontSizeOutlined, AntDesignOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { cardsInformationApi } from 'api/card-information';
import { updateData } from 'utils/updateData';
import { createData } from 'utils/createData';

import './Form.scss';

export default function FormCardInformation({
  isNew = false,
  card = {},
  setIsVisibleModal,
  setReloadCards,
}) {
  const [form] = Form.useForm();
  const { _id } = card;

  useEffect(() => {
    form.setFieldsValue({
      title: card?.title || '',
      description: card?.description || '',
      icon: card?.icon || '',
    });
  }, [card, form]);

  const handleSubmit = async (values) => {
    let result = false;
    if (isNew) {
      result = await createData(cardsInformationApi.create, values);
    } else {
      result = await updateData(cardsInformationApi.update(_id), values);
    }

    setIsVisibleModal(!result);
    setReloadCards(result);
    !result && form.resetFields();
  };

  return (
    <div className="card-information-form">
      <Form form={form} className="course-form" onFinish={handleSubmit}>
        <Form.Item name="title" rules={[{ required: true, message: 'Please input a title!' }]}>
          <Input prefix={<FontSizeOutlined />} placeholder="titulo" />
        </Form.Item>
        <Form.Item
          name="description"
          rules={[{ required: true, message: 'Please input a description!' }]}
        >
          <Input prefix={<InfoCircleOutlined />} placeholder="descripción" />
        </Form.Item>
        <Form.Item name="icon" rules={[{ required: true, message: 'Please input a icon design!' }]}>
          <Input prefix={<AntDesignOutlined />} placeholder="Icon AntDesing" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="btn-submit">
            {isNew ? 'Guardar' : 'Editar'}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
