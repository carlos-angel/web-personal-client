import React from 'react';
import { List, Button } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';

import { cardsInformationApi } from 'api/card-information';
import { deleteConfirm } from 'utils/deleteConfirm';

export default function Item({ card, setReloadCards, editCard }) {
  const showDeleteConfirm = (id, title) => deleteConfirm(
    `la tarjeta ${title}`,
    cardsInformationApi.delete(id),
    setReloadCards,
  );

  const { _id } = card;

  return (
    <List.Item
      actions={[
        <Button type="primary" onClick={() => editCard(card)}>
          <EditOutlined />
        </Button>,
        <Button
          type="danger"
          onClick={() => showDeleteConfirm(_id, card.title)}
        >
          <DeleteOutlined />
        </Button>,
      ]}
    >
      <List.Item.Meta title={card.title} description={card.description} />
    </List.Item>
  );
}
