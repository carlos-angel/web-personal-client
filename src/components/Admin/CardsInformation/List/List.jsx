import React from 'react';
import { List } from 'antd';

import './List.scss';

export default function CardsInformationList({ cards, render }) {
  return !cards ? null : (
    <div className="cards-information-list">
      <List
        dataSource={cards}
        renderItem={render}
      />
    </div>
  );
}
