import React, { useCallback } from 'react';
import { Avatar as AvatarAntd, Image } from 'antd';
import { useDropzone } from 'react-dropzone';

import { useImage } from 'hooks/useImage';
import { updateImage } from 'utils/updateImage';

import NotAvatar from 'assets/images/png/no-avatar.png';
import NotImage from 'assets/images/png/notFound.png';
import './Avatar.scss';

function Avatar({
  id,
  getImageUrl,
  updateImageUrl,
  setReloadList,
  circle = false,
  isAvatar = false,
}) {
  const [image, setImage] = useImage(getImageUrl);

  const onDrop = useCallback(
    (acceptedFiles) => {
      const file = acceptedFiles[0];
      setImage(URL.createObjectURL(file));
      const newNameFile = updateImage(updateImageUrl, {
        image: file,
        typeImage: isAvatar ? 'avatar' : 'image',
      });
      newNameFile && setReloadList(true);
    },
    [id],
  );

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: 'image/jpg,image/jpeg,image/png',
    noKeyboard: true,
    onDrop,
  });

  return (
    <div className={`upload-image ${circle && 'avatar'}`} {...getRootProps()}>
      <input {...getInputProps()} />
      {circle ? (
        <AvatarAntd
          size={150}
          src={isDragActive ? NotAvatar : image || NotAvatar}
        />
      ) : (
        <Image
          size={150}
          src={isDragActive ? NotImage : image || NotImage}
          preview={{ visible: false, mask: false }}
        />
      )}
    </div>
  );
}

export default Avatar;
