import React from 'react';
import { List } from 'antd';

import './ReviewList.scss';

export default function ReviewList({ reviews, render }) {
  return !reviews ? null : (
    <div className="reviews-list">
      <List dataSource={reviews} renderItem={render} />
    </div>
  );
}
