import React, { useState } from 'react';
import { Avatar, List, Button } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import Modal from 'components/Modal';
import Form from 'components/Admin/Reviews/Form';
import { apiReview } from 'api/review.api';
import { useImage } from 'hooks/useImage';
import NotAvatar from 'assets/images/png/no-avatar.png';
import { deleteConfirm } from 'utils/deleteConfirm';

export default function Review({ review, setReloadList }) {
  const [avatar] = useImage(apiReview.getImage(review.image));
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const { _id } = review;

  const showDeleteConfirm = (id, title) => {
    deleteConfirm(`review ${title}`, apiReview.delete(id), setReloadList);
  };

  const renderModal = () => (
    <Modal
      title={`Editar review ${review.name}`}
      isVisible={isVisibleModal}
      setIsVisible={setIsVisibleModal}
    >
      <Form
        review={review}
        setIsVisibleModal={setIsVisibleModal}
        setReloadList={setReloadList}
      />
    </Modal>
  );

  return (
    <>
      <List.Item
        actions={[
          <Button type="primary" onClick={() => setIsVisibleModal(true)}>
            <EditOutlined />
          </Button>,
          <Button
            type="danger"
            onClick={() => showDeleteConfirm(_id, review.name)}
          >
            <DeleteOutlined />
          </Button>,
        ]}
      >
        <List.Item.Meta
          avatar={<Avatar src={review.image ? avatar : NotAvatar} />}
          title={review.name}
          description={review.review}
        />
      </List.Item>
      {isVisibleModal && renderModal()}
    </>
  );
}
