import React, { useEffect } from 'react';
import { Form, Input, Button } from 'antd';
import { UserOutlined, CoffeeOutlined, MessageOutlined } from '@ant-design/icons';
import Avatar from 'components/Admin/Avatar';
import { apiReview } from 'api/review.api';
import { updateData } from 'utils/updateData';
import { createData } from 'utils/createData';

import './Form.scss';

export default function ReviewForm({
  review = {},
  isNew = false,
  setIsVisibleModal,
  setReloadList,
}) {
  const [form] = Form.useForm();
  const { _id } = review;

  useEffect(() => {
    form.setFieldsValue({
      name: review?.name || '',
      jobTitle: review?.jobTitle || '',
      review: review?.review || '',
    });
  }, [review, form]);

  const handleSubmit = async (values) => {
    let result = false;
    if (isNew) {
      result = await createData(apiReview.create, values);
    } else {
      result = await updateData(apiReview.update(_id), values);
    }

    setIsVisibleModal(!result);
    setReloadList(result);
    !result && form.resetFields();
  };

  return (
    <div className="review-form">
      {!isNew && (
        <Avatar
          getImageUrl={apiReview.getImage(review.image)}
          updateImageUrl={apiReview.updateImage(_id)}
          id={_id}
          setReloadList={setReloadList}
          circle
        />
      )}
      <Form form={form} onFinish={handleSubmit}>
        <Form.Item name="name" rules={[{ required: true, message: 'Please input a name!' }]}>
          <Input prefix={<UserOutlined />} placeholder="Nombre" />
        </Form.Item>
        <Form.Item
          name="jobTitle"
          rules={[{ required: true, message: 'Please input a job title!' }]}
        >
          <Input prefix={<CoffeeOutlined />} placeholder="Trabajo o titulo" />
        </Form.Item>
        <Form.Item name="review" rules={[{ required: true, message: 'Please input a review!' }]}>
          <Input prefix={<MessageOutlined />} placeholder="Reseña" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="btn-submit">
            {isNew ? 'Guardar' : 'Editar'}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
