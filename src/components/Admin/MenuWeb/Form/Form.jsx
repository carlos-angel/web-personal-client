import React, { useEffect } from 'react';
import {
  Form, Input, Button, Select,
} from 'antd';
import { LinkOutlined, TagOutlined } from '@ant-design/icons';
import { menuApi } from 'api/menu';
import { updateData } from 'utils/updateData';
import { createData } from 'utils/createData';

import './Form.scss';

export default function MenuWebForm({
  data = { url: '', linkType: undefined },
  isNew = false,
  setIsVisibleModal,
  setReloadList,
  nextOrder,
}) {
  const { Option } = Select;
  const [form] = Form.useForm();
  const { _id } = data;

  const splitLink = (url) => {
    const findHttp = url.search('http');
    if (findHttp >= 0) {
      const split = url.split('//');
      const linkType = `${split[0]}//`;
      const link = split[1];
      return {
        linkType,
        url: link,
      };
    }
    const linkType = '/';
    const link = url.split('/')[1];
    return {
      linkType,
      url: link,
    };
  };

  useEffect(() => {
    const { url, linkType } = splitLink(data.url);
    form.setFieldsValue({
      title: data?.title || '',
      url,
      linkType,
    });
  }, [data, form]);

  const onFinish = async ({ title, url, linkType }) => {
    let result = false;
    const values = {
      title,
      url: `${linkType}${url}`,
      order: nextOrder,
    };
    if (isNew) {
      result = await createData(menuApi.create, values);
    } else {
      result = await updateData(menuApi.update(_id), values);
    }

    setIsVisibleModal(!result);
    setReloadList(result);
    !result && form.resetFields();
  };

  const selectBefore = (
    <Form.Item
      name="linkType"
      noStyle
      rules={[{ required: true, message: 'Please input a link type!' }]}
    >
      <Select placeholder="link type" style={{ width: 110 }}>
        <Option value="/">my page</Option>
        <Option value="http://">http://</Option>
        <Option value="https://">https://</Option>
      </Select>
    </Form.Item>
  );

  return (
    <Form className="menu-form" form={form} onFinish={onFinish}>
      <Form.Item name="title" rules={[{ required: true, message: 'Please input a title!' }]}>
        <Input prefix={<TagOutlined />} placeholder="Nombre" />
      </Form.Item>

      <Form.Item name="url" rules={[{ required: true, message: 'Please input a link!' }]}>
        <Input prefix={<LinkOutlined />} placeholder="link" addonBefore={selectBefore} />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="btn-submit">
          {isNew ? 'Guardar' : 'Editar'}
        </Button>
      </Form.Item>
    </Form>
  );
}
