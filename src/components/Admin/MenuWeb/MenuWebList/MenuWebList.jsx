import React from 'react';
import { notification } from 'antd';
import DragSortableList from 'react-drag-sortable';
import { menuApi } from 'api/menu';
import { updateDataApi } from 'api';

import './MenuWebList.scss';

export default function MenuWebList({ menu, renderItem }) {
  const onSort = (sortedList) => {
    sortedList.forEach(async (item) => {
      const { _id } = item.content.props.item;
      const url = menuApi.update(_id);
      const order = item.rank;
      await updateDataApi(url, { order });
    });

    notification.success({ message: 'orden actualizado' });
  };

  return (
    <div className="menu-web-list__items">
      <DragSortableList
        items={menu.map((item) => ({ content: renderItem(item) }))}
        onSort={onSort}
        type="vertical"
      />
    </div>
  );
}
