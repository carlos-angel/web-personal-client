import React from 'react';
import { Switch, List, Button } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { menuApi } from 'api/menu';
import { deleteConfirm } from 'utils/deleteConfirm';
import { active } from 'utils/active';

export default function MenuWeb({ item, editMenu, setReloadMenuWeb }) {
  const showDeleteConfirm = (id, title) => deleteConfirm(`curso ${title}`, menuApi.remove(id), setReloadMenuWeb);
  const { _id } = item;
  return (
    <List.Item
      actions={[
        <Switch
          defaultChecked={item.active}
          onChange={(e) => active(menuApi.active(_id), e)}
        />,
        <Button type="primary" onClick={() => editMenu(item)}>
          <EditOutlined />
        </Button>,
        <Button
          type="danger"
          onClick={() => showDeleteConfirm(_id, item.title)}
        >
          <DeleteOutlined />
        </Button>,
      ]}
    >
      <List.Item.Meta title={item.title} description={item.url} />
    </List.Item>
  );
}
