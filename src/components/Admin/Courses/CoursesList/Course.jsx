import React from 'react';
import { List, Button, Avatar } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';

import { coursesApi } from 'api/courses';
import { useImage } from 'hooks/useImage';
import { deleteConfirm } from 'utils/deleteConfirm';

import NotFound from 'assets/images/png/notFound.png';

export default function Course({ course, setReloadCourses, handleEditCourse }) {
  const [image] = useImage(coursesApi.getImage(course.image));
  const { _id } = course;

  const showDeleteConfirm = (id, title) => deleteConfirm(`curso ${title}`, coursesApi.delete(id), setReloadCourses);

  return (
    <List.Item
      actions={[
        <Button type="primary" onClick={() => handleEditCourse(course)}>
          <EditOutlined />
        </Button>,
        <Button
          type="danger"
          onClick={() => showDeleteConfirm(_id, course.title)}
        >
          <DeleteOutlined />
        </Button>,
      ]}
    >
      <List.Item.Meta
        title={course.title}
        description={course.description}
        avatar={<Avatar src={course.image ? image : NotFound} />}
      />
    </List.Item>
  );
}
