import React from 'react';
import { List } from 'antd';
import './CoursesList.scss';

export default function CoursesList({ courses, render, isEmpty }) {
  return (
    <div className="courses-list__items">
      {!courses === 0 ? (
        isEmpty()
      ) : (
        <List dataSource={courses} renderItem={render} />
      )}
    </div>
  );
}
