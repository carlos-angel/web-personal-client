import React, { useEffect } from 'react';
import {
  Form, Input, Button, Select, Checkbox,
} from 'antd';
import { FontSizeOutlined, GiftOutlined, LinkOutlined } from '@ant-design/icons';
import Avatar from 'components/Admin/Avatar';
import { coursesApi } from 'api/courses';
import { updateData } from 'utils/updateData';
import { createData } from 'utils/createData';

import './Form.scss';

export default function CourseForm({
  isNew = false, data = {}, setIsVisibleModal, setReloadList,
}) {
  const [form] = Form.useForm();
  const { Option } = Select;
  const { _id } = data;

  useEffect(() => {
    form.setFieldsValue({
      title: data?.title || '',
      description: data?.description || '',
      type: data?.type || undefined,
      link: data?.link || '',
      coupon: data?.coupon || '',
      free: data?.free || false,
    });
  }, [data, form]);

  const onFinish = async (values) => {
    let result = false;
    if (isNew) {
      result = await createData(coursesApi.create, values);
    } else {
      result = await updateData(coursesApi.update(_id), values);
    }

    setIsVisibleModal(!result);
    setReloadList(result);
    !result && form.resetFields();
  };
  return (
    <>
      {!isNew && (
        <Avatar
          getImageUrl={coursesApi.getImage(data.image)}
          updateImageUrl={coursesApi.updateImage(_id)}
          id={_id}
          setReloadList={setReloadList}
        />
      )}
      <Form className="course-form" form={form} onFinish={onFinish}>
        <Form.Item name="title" rules={[{ required: true, message: 'Please input a title!' }]}>
          <Input prefix={<FontSizeOutlined />} placeholder="titulo" />
        </Form.Item>
        <Form.Item
          name="description"
          rules={[{ required: true, message: 'Please input a description!' }]}
        >
          <Input prefix={<FontSizeOutlined />} placeholder="Descripción" />
        </Form.Item>

        <Form.Item name="type" rules={[{ required: true, message: 'Please selected a platform!' }]}>
          <Select placeholder="plataforma">
            <Option value="udemy">Plataforma Udemy</Option>
            <Option value="youtube">Plataforma Youtube</Option>
          </Select>
        </Form.Item>

        <Form.Item name="link" rules={[{ required: true, message: 'Please input a link!' }]}>
          <Input prefix={<LinkOutlined />} placeholder="link del curso" />
        </Form.Item>

        <Form.Item name="coupon" rules={[{ required: false }]}>
          <Input prefix={<GiftOutlined />} placeholder="cupón de descuento" />
        </Form.Item>

        <Form.Item name="free" valuePropName="checked" rules={[{ required: false }]}>
          <Checkbox>Curso Gratis</Checkbox>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="btn-submit">
            {isNew ? 'Guardar' : 'Editar'}
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}
