import { notification } from 'antd';
import { activeApi } from 'api';

export const active = async (url, isActive) => {
  try {
    const result = await activeApi(url, isActive);
    const success = !result?.error || false;
    const status = success ? 'success' : 'error';

    notification[status]({
      message: result?.message || 'Internal server error',
    });
    return success;
  } catch (error) {
    notification.error({ message: 'Internal server error' });
    return false;
  }
};
