import { notification } from 'antd';
import { createDataApi } from '../api';

export const createData = async (url, body) => {
  try {
    const data = await createDataApi(url, body);
    const typeNotification = !data.error ? 'success' : 'error';
    notification[typeNotification]({ message: data.message });
    return !data.error;
  } catch (error) {
    notification.error({ message: error.message });
    return false;
  }
};
