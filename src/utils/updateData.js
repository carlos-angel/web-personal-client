import { notification } from 'antd';
import { updateDataApi } from 'api';

export const updateData = async (url, body) => {
  try {
    const result = await updateDataApi(url, body);
    const success = !result?.error || false;
    const status = success ? 'success' : 'error';

    notification[status]({
      message: result?.message || 'Internal server error',
    });
    return success;
  } catch (error) {
    notification.error({ message: 'Internal server error' });
    return false;
  }
};
