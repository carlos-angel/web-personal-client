import { notification } from 'antd';
import { updateImageApi } from 'api';

export const updateImage = async (url, image) => {
  try {
    const result = await updateImageApi(url, image);
    const success = !result?.error;
    const status = success ? 'success' : 'error';

    notification[status]({
      message: result?.message || 'Image no updated',
    });
    return success;
  } catch (error) {
    notification.error({ message: 'Internal server error' });
    return false;
  }
};
