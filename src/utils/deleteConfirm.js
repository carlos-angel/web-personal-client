import { Modal, notification } from 'antd';
import { deleteData } from '../api';

const { confirm } = Modal;

export const deleteConfirm = (title, url, setReload) => {
  confirm({
    title: `eliminar ${title}`,
    content: 'Una vez eliminado ya no podrás recuperarlo ¿Estás seguro de continuar?',
    okText: 'eliminar',
    okType: 'danger',
    cancelText: 'Cancelar',
    onOk() {
      deleteData(url)
        .then((response) => {
          const status = !response.error ? 'success' : 'error';
          notification[status]({ message: response.message });
          setReload(!response.error);
        })
        .catch((err) => notification.error({
          message: err.message || 'Internal server error',
        }));
    },
  });
};
