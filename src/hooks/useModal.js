import { useState } from 'react';

export const useModal = () => {
  const [isVisible, setIsVisible] = useState(false);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState(null);

  return {
    properties: { isVisible, title, content },
    setIsVisible,
    setTitle,
    setContent,
  };
};
