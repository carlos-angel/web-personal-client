import { useState, useEffect } from 'react';

export const useAvatar = (avatar, getImageApi) => {
  const [image, setImage] = useState(null);

  useEffect(() => {
    if (avatar) {
      getImageApi({ avatar }).then((response) => setImage(response));
    } else {
      setImage(null);
    }
  }, [avatar]);

  const setAvatar = (avatarImage) => setImage(avatarImage);

  return [image, setAvatar];
};
