import { useState, useEffect } from 'react';
import { getData } from 'api';

export const useData = (urlApi, token = false) => {
  const [url, setUrl] = useState(urlApi);
  const [reloadData, setReloadData] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    getData(url, token)
      .then((response) => setData(response))
      .catch(() => setData([]));
    setReloadData(false);
  }, [reloadData, url, token]);

  const updateData = () => setReloadData(true);

  const updateUrlApi = (path = urlApi) => setUrl(path);

  return [data, updateData, updateUrlApi];
};
