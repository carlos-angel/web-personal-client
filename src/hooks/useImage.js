import { useState, useEffect } from 'react';
import { getImageApi } from 'api';
import NotFound from 'assets/images/png/notFound.png';

export const useImage = (url) => {
  const [image, setImage] = useState(null);

  useEffect(() => {
    if (url) {
      getImageApi(url)
        .then((urlImage) => setImage(urlImage))
        .catch(() => setImage(NotFound));
    } else {
      setImage(null);
    }
  }, [url]);

  return [image, setImage];
};
