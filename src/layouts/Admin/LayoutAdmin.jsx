import React, { useState } from 'react';
import { Layout } from 'antd';
import { Route, Redirect } from 'react-router-dom';
import LoadRoutes from '../../routes/LoadRoutes';
import useAuth from '../../hooks/useAuth';
import MenuTop from './MenuTop';
import MenuSideBar from './MenuSideBar';
import AdminSignIn from '../../pages/Admin/SignIn';

import './LayoutAdmin.scss';

export default function LayoutAdmin({ routes }) {
  const { Header, Content, Footer } = Layout;
  const [menuCollapsed, setMenuCollapsed] = useState(false);

  const { user, isLoading } = useAuth();

  if (!user && !isLoading) {
    return (
      <>
        <Route path="/admin/login" component={AdminSignIn} />
        <Redirect to="/admin/login" />
      </>
    );
  }

  if (user && !isLoading) {
    return (
      <Layout>
        <MenuSideBar menuCollapsed={menuCollapsed} />
        <Layout className="layout-admin" style={{ marginLeft: menuCollapsed ? '80px' : '200px' }}>
          <Header className="layout-admin__header">
            <MenuTop menuCollapsed={menuCollapsed} setMenuCollapsed={setMenuCollapsed} />
          </Header>
          <Content className="layout-admin__content">
            <LoadRoutes routes={routes} />
          </Content>
          <Footer className="layout-admin__footer">Carlos alberto angel angel</Footer>
        </Layout>
      </Layout>
    );
  }
  return null;
}
