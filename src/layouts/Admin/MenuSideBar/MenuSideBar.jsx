import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import {
  HomeOutlined,
  MenuOutlined,
  TeamOutlined,
  CoffeeOutlined,
  CommentOutlined,
  InfoCircleOutlined,
  IdcardOutlined,
} from '@ant-design/icons';

import './MenuSideBar.scss';

function MenuSideBar({ menuCollapsed, location }) {
  const { Sider } = Layout;
  return (
    <Sider className="admin-sider" collapsed={menuCollapsed}>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={[location.pathname]}>
        <Menu.Item key="/admin">
          <Link to="/admin">
            <HomeOutlined />
            <span className="nav-text">Home</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/admin/users">
          <Link to="/admin/users">
            <TeamOutlined />
            <span className="nav-text">users</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/admin/menu">
          <Link to="/admin/menu">
            <MenuOutlined />
            <span className="nav-text">menu</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/admin/courses">
          <Link to="/admin/courses">
            <CoffeeOutlined />
            <span className="nav-text">cursos</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/admin/blog">
          <Link to="/admin/blog">
            <CommentOutlined />
            <span className="nav-text">Blog</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/admin/card-information">
          <Link to="/admin/card-information">
            <InfoCircleOutlined />
            <span className="nav-text">Cards Information</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/admin/reviews">
          <Link to="/admin/reviews">
            <IdcardOutlined />
            <span className="nav-text">Reviews</span>
          </Link>
        </Menu.Item>
      </Menu>
    </Sider>
  );
}

export default withRouter(MenuSideBar);
