import React, { useState } from 'react';
import {
  Form, Input, Button, notification,
} from 'antd';
import { UserOutlined } from '@ant-design/icons';

import { suscribeEmailApi } from '../../../api/newsletter';

import './Newsletter.scss';

export default function Newsletter() {
  const [email, setEmail] = useState('');
  const emailValid = /^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  const onSubmit = (e) => {
    e.preventDefault();
    if (!emailValid.test(email)) {
      notification.error({ message: 'el email es obligatorio' });
      return;
    }

    suscribeEmailApi(email)
      .then((response) => {
        const status = !response.error ? 'success' : 'warning';
        notification[status]({ message: response.message });
        if (status === 'success') setEmail('');
      })
      .catch((errorMessage) => notification.error({ message: errorMessage }));
  };

  return (
    <div className="newsletter">
      <h3>Newsletter</h3>
      <Form onSubmitCapture={onSubmit}>
        <Form.Item>
          <Input
            prefix={<UserOutlined />}
            placeholder="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            ¡Me suscribo!
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
