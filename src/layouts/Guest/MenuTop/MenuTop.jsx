import React from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';

import SocialLinks from '../SocialLinks';
import { menuApi } from '../../../api/menu';
import { useData } from '../../../hooks/useData';

import Logo from '../../../assets/images/png/logo-white.png';
import './MenuTop.scss';

export default function MenuTop() {
  const [menus] = useData(menuApi.getAll);

  return (
    <Menu className="menu-web-top" mode="horizontal">
      <Menu.Item className="menu-web-top__logo">
        <Link to="/">
          <img src={Logo} alt="logo" />
          {' '}
          CodeTime
        </Link>
      </Menu.Item>

      {menus.map((item) => {
        const { _id, url, title } = item;
        const external = item.url.indexOf('http') > -1;

        if (external) {
          return (
            <Menu.Item key={_id} className="menu-top__item">
              <a href={url} target="_blank" rel="noopener noreferrer">
                {title}
              </a>
            </Menu.Item>
          );
        }
        return (
          <Menu.Item key={_id} className="menu-top__item">
            <Link to={url}>{title}</Link>
          </Menu.Item>
        );
      })}
      <SocialLinks />
      <Menu.Item>
        <Link className="menu-web-top__login" to="/admin/login">
          Login
        </Link>
      </Menu.Item>
    </Menu>
  );
}
