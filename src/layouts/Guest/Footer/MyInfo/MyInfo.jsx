import React from 'react';

import SocialLinks from '../../SocialLinks';

import Logo from '../../../../assets/images/png/logo-white.png';
import './MyInfo.scss';

export default function MyInfo() {
  return (
    <div className="my-info">
      <div className="logo">
        <img src={Logo} alt="logo" />
        {' '}
        <span> CodeTime </span>
      </div>
      <h4>
        Entra en el mundo del desarrollo web, disfruta creando proyectos de todo
        tipo, deja que tú imaginación fluya y crea verdaderas maravillas!
      </h4>
      <SocialLinks />
    </div>
  );
}
