import React from 'react';
import { Row, Col } from 'antd';
import LoadRoutes from '../../routes/LoadRoutes';

import MenuTop from './MenuTop';
import Footer from './Footer';

export default function LayoutGuestBasic({ routes }) {
  return (
    <>
      <Row>
        <Col md={4} />
        <Col md={16}>
          <MenuTop />
        </Col>
        <Col md={4} />
      </Row>
      <LoadRoutes routes={routes} />
      <Footer>Carlos Alberto Angel Angel</Footer>
    </>
  );
}
