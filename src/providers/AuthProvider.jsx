import React, { useState, useEffect, createContext } from 'react';
import {
  getAccessToken,
  getRefreshToken,
  refreshAccessTokenApi,
  logout,
  decodeToken,
} from '../api/auth';

export const AuthContext = createContext();

function checkUserLogin(setUser) {
  const accessToken = getAccessToken();
  if (!accessToken) {
    const refreshToken = getRefreshToken();
    if (!refreshToken) {
      logout();
      setUser({ user: null, isLoading: false });
    } else {
      refreshAccessTokenApi(refreshToken);
    }
  } else {
    setUser({
      isLoading: false,
      user: decodeToken(accessToken),
    });
  }
}

export default function AuthProvider({ children }) {
  const [user, setUser] = useState({ user: null, isLoading: true });

  useEffect(() => {
    checkUserLogin(setUser);
  }, []);

  return <AuthContext.Provider value={user}>{children}</AuthContext.Provider>;
}
