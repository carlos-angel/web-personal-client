import { BASE_PATH } from './config';

export const menuApi = {
  getAll: `${BASE_PATH}/v1/menu`,
  create: `${BASE_PATH}/v1/menu/new`,
  update: (id) => `${BASE_PATH}/v1/menu/${id}/update`,
  active: (id) => `${BASE_PATH}/v1/menu/${id}/activate`,
  remove: (id) => `${BASE_PATH}/v1/menu/${id}/delete`,
};
