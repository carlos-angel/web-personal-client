import { BASE_PATH } from './config';

export const postsApi = {
  getAll: (page) => `${BASE_PATH}/v1/post/all?page=${page}`,
  delete: (id) => `${BASE_PATH}/v1/post/${id}/delete`,
  create: `${BASE_PATH}/v1/post/new`,
  update: (id) => `${BASE_PATH}/v1/post/${id}/update`,
  active: (id) => `${BASE_PATH}/v1/post/${id}/publish`,
};

export function getAllPostsApi(token, page) {
  const url = `${BASE_PATH}/v1/post/all?page=${page}`;
  const params = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: token,
    },
  };
  return fetch(url, params)
    .then((response) => response.json())
    .then((result) => result)
    .catch((err) => (err.message ? err.message : 'Internal server error'));
}

export function getPublishPostsApi(page) {
  const url = `${BASE_PATH}/v1/post/?page=${page}&active=true`;
  return fetch(url)
    .then((response) => response.json())
    .then((result) => result)
    .catch((err) => (err.message ? err.message : 'Internal server error'));
}

export function getPostApi(urlPost) {
  const url = `${BASE_PATH}/v1/post/${urlPost}`;
  return fetch(url)
    .then((response) => response.json())
    .then((result) => result)
    .catch((err) => (err.message ? err.message : 'Internal server error'));
}
