import { BASE_PATH } from './config';

export const coursesApi = {
  getAll: `${BASE_PATH}/v1/courses`,
  getOffers: `${BASE_PATH}/v1/courses/4/offer`,
  create: `${BASE_PATH}/v1/courses/new`,
  delete: (id) => `${BASE_PATH}/v1/courses/${id}/delete`,
  getImage: (image) => (image ? `${BASE_PATH}/v1/courses/${image}/image` : false),
  update: (id) => `${BASE_PATH}/v1/courses/${id}/update`,
  updateImage: (id) => `${BASE_PATH}/v1/courses/${id}/upload-image`,
};

export function getCoursesDataUdemyApi(courseId) {
  const url = `https://www.udemy.com/api-2.0/courses/${courseId}/?fields[course]=title,headline,url,price,image_480x270`;

  return fetch(url)
    .then(async (response) => ({
      code: response.status,
      data: await response.json(),
    }))
    .then((result) => result)
    .catch((err) => err);
}
