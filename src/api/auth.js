import jwtDecode from 'jwt-decode';
import { BASE_PATH } from './config';
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../utils/constans';

function willExpireToken(token) {
  const secods = 60;
  const metaToken = jwtDecode(token);
  const { exp } = metaToken;
  const now = (Date.now() + secods) / 1000;
  return now > exp;
}

export function getAccessToken() {
  const accessToken = localStorage.getItem(ACCESS_TOKEN);
  return !accessToken || accessToken === 'null' || willExpireToken(accessToken)
    ? null
    : accessToken;
}

export function getRefreshToken() {
  const refreshToken = localStorage.getItem(REFRESH_TOKEN);
  return !refreshToken || refreshToken === 'null' || willExpireToken(refreshToken) // prettier-ignore
    ? null
    : refreshToken;
}

export function login({ accessToken, refreshToken }) {
  localStorage.setItem(ACCESS_TOKEN, accessToken);
  localStorage.setItem(REFRESH_TOKEN, refreshToken);
}

export function logout() {
  localStorage.removeItem(ACCESS_TOKEN);
  localStorage.removeItem(REFRESH_TOKEN);
}

export function refreshAccessTokenApi(refreshToken) {
  const url = `${BASE_PATH}/v1/auth/refresh-access-token`;
  const params = {
    method: 'POST',
    body: { refreshToken },
    headers: { 'Content-Type': 'application/json' },
  };

  fetch(url, params)
    .then((response) => response.json())
    .then((result) => (result ? login(result.data) : logout()))
    .catch((err) => (err.message ? err.message : 'Internal server error'));
}

export function decodeToken(token) {
  return jwtDecode(token);
}
