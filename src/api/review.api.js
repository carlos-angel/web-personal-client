import { BASE_PATH } from './config';

export const apiReview = {
  getReviews: `${BASE_PATH}/v1/review`,
  create: `${BASE_PATH}/v1/review/new`,
  delete: (id) => `${BASE_PATH}/v1/review/${id}/delete`,
  update: (id) => `${BASE_PATH}/v1/review/${id}/update`,
  getImage: (image) => (image ? `${BASE_PATH}/v1/review/${image}/image` : false),
  updateImage: (id) => `${BASE_PATH}/v1/review/${id}/upload-image`,
};
