import axios from 'axios';
import { getAccessToken } from './auth';

export async function getData(url, token = false) {
  const {
    data: { data },
  } = await axios.get(url, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: token ? getAccessToken() : '',
    },
  });

  return data || [];
}

export async function createDataApi(url, body) {
  const { data } = await axios.post(url, body, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: getAccessToken(),
    },
  });

  return data;
}

export async function deleteData(url) {
  const { data } = await axios.delete(url, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: getAccessToken(),
    },
  });

  return data;
}

export async function updateDataApi(url, body) {
  const { data } = await axios.put(url, body, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: getAccessToken(),
    },
  });

  return data;
}

export async function getImageApi(url) {
  const file = await axios.get(url);
  return file.config.url;
}

export async function updateImageApi(url, { image, typeImage }) {
  const formData = new FormData();
  formData.append(typeImage, image, image.name);

  const { data } = await axios.put(url, formData, {
    headers: {
      Authorization: getAccessToken(),
    },
  });

  return data;
}

export async function activeApi(url, active) {
  const { data } = await axios.put(
    url,
    { active },
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: getAccessToken(),
      },
    },
  );

  return data;
}
