import { BASE_PATH } from './config';

export function suscribeEmailApi(email) {
  const url = `${BASE_PATH}/v1/newsletter/${email}/suscribe`;

  const params = {
    method: 'POST',
  };

  return fetch(url, params)
    .then((response) => response.json())
    .then((result) => result)
    .catch((err) => (err.message ? err.message : 'Internal server error'));
}
