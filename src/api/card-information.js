import { BASE_PATH } from './config';

export const cardsInformationApi = {
  getCards: `${BASE_PATH}/v1/card-information`,
  create: `${BASE_PATH}/v1/card-information/new`,
  delete: (id) => `${BASE_PATH}/v1/card-information/${id}/delete`,
  update: (id) => `${BASE_PATH}/v1/card-information/${id}/update`,
};
