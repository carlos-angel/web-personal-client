import { BASE_PATH } from './config';

export const userApi = {
  create: `${BASE_PATH}/v1/auth/sign-up`,
  delete: (id) => `${BASE_PATH}/v1/users/${id}/delete`,
  update: (id) => `${BASE_PATH}/v1/users/${id}/update`,
  getImage: (image) => (image ? `${BASE_PATH}/v1/users/${image}/avatar` : false),
  updateImage: (id) => `${BASE_PATH}/v1/users/${id}/upload-avatar`,
  active: (id) => `${BASE_PATH}/v1/users/${id}/activate`,
  usersByActive: (active) => `${BASE_PATH}/v1/users/active?active=${active}`,
};

export function signInApi({ email, password }) {
  const url = `${BASE_PATH}/v1/auth/sign-in`;

  const params = {
    method: 'POST',
    body: JSON.stringify({ email, password }),
    headers: {
      'Content-Type': 'application/json',
    },
  };

  return fetch(url, params)
    .then((response) => response.json())
    .then((result) => result)
    .catch((err) => (err.message ? err.message : 'Internal server error'));
}
