import React, { useState } from 'react';
import { Button } from 'antd';
import { apiReview } from 'api/review.api';
import ReviewsList from 'components/Admin/Reviews/ReviewsList';
import Review from 'components/Admin/Reviews/ReviewsList/Review';
import Form from 'components/Admin/Reviews/Form';
import Modal from 'components/Modal';
import { useData } from 'hooks/useData';

import './Reviews.scss';

export default function CardsInformation() {
  const [reviews, setReloadReviews] = useData(apiReview.getReviews);
  const [isVisibleModal, setIsVisibleModal] = useState(false);

  const renderModal = () => (
    <Modal
      title="Nuevo Review"
      isVisible={isVisibleModal}
      setIsVisible={setIsVisibleModal}
    >
      <Form
        isNew
        setIsVisibleModal={setIsVisibleModal}
        setReloadList={setReloadReviews}
      />
    </Modal>
  );

  return (
    <div className="reviews">
      <div className="reviews__header">
        <Button type="primary" onClick={() => setIsVisibleModal(true)}>
          Nueva tarjeta
        </Button>
      </div>

      <ReviewsList
        reviews={reviews}
        render={(review) => (
          <Review review={review} setReloadList={setReloadReviews} />
        )}
      />
      {isVisibleModal && renderModal()}
    </div>
  );
}
