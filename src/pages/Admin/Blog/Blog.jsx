import React, { useEffect, createContext } from 'react';
import { Button } from 'antd';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';

import Modal from 'components/Modal';
import PostList from 'components/Admin/Blog/PostsList';
import Post from 'components/Admin/Blog/PostsList/Post';
import Pagination from 'components/Shared/Pagination';
import { postsApi } from 'api/post';
import PostForm from 'components/Admin/Blog/Form';
import { useData } from 'hooks/useData';
import { useModal } from 'hooks/useModal';

import './Blog.scss';

export const BlogContext = createContext({});
const { Provider } = BlogContext;

function Blog({ history, location }) {
  const { page = 1 } = queryString.parse(location.search);

  const [posts, setReloadPosts, updateUrlApi] = useData(postsApi.getAll(page), true);

  useEffect(() => {
    updateUrlApi(postsApi.getAll(page));
  }, [updateUrlApi, page]);

  const {
    properties: { isVisible, title, content },
    setIsVisible,
    setTitle,
    setContent,
  } = useModal();

  const addPost = () => {
    setIsVisible(true);
    setTitle('Nuevo post');
    setContent(<PostForm isNew />);
  };

  if (!posts) {
    return null;
  }

  return (
    <Provider
      value={{
        setIsVisibleModal: setIsVisible,
        setTitleModal: setTitle,
        setContentModal: setContent,
        setReloadList: setReloadPosts,
      }}
    >
      <div className="blog">
        <div className="blog__add-post">
          <Button type="primary" onClick={addPost}>
            Nuevo post
          </Button>
        </div>
        <PostList posts={posts.docs} render={(post) => <Post post={post} />} />

        <Pagination
          location={location}
          history={history}
          total={posts.total || 0}
          defaultCurrent={parseInt(posts.page, 10)}
          limit={posts.limit || 0}
        />

        <Modal title={title} isVisible={isVisible} setIsVisible={setIsVisible} width="75%">
          {content}
        </Modal>
      </div>
    </Provider>
  );
}

export default withRouter(Blog);
