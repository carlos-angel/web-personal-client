import React, { useState, useEffect } from 'react';
import { Switch, Button } from 'antd';
import { userApi } from 'api/user';
import UsersList from 'components/Admin/Users/UsersList';
import User from 'components/Admin/Users/UsersList/User';
import Modal from 'components/Modal/Modal';
import Form from 'components/Admin/Users/Form';
import { useModal } from 'hooks/useModal';
import { useData } from 'hooks/useData';
import './Users.scss';

export default function Users() {
  const [viewUsersActive, setViewUsersActive] = useState(true);
  const [users, setReloadUsers, usersByActive] = useData(
    userApi.usersByActive(viewUsersActive),
    true,
  );
  const {
    properties, setIsVisible, setTitle, setContent,
  } = useModal();

  const addUserModal = () => {
    setIsVisible(true);
    setTitle('crear nuevo usuario');
    setContent(
      <Form
        isNew
        setIsVisibleModal={setIsVisible}
        setReloadList={setReloadUsers}
      />,
    );
  };

  const editUser = (user) => {
    setTitle(
      `Editar al usuario ${user.lastname ? user.lastname : '...'} ${
        user.name ? user.name : '...'
      }`,
    );
    setIsVisible(true);
    setContent(
      <Form
        data={user}
        setIsVisibleModal={setIsVisible}
        setReloadList={setReloadUsers}
      />,
    );
  };

  useEffect(() => {
    usersByActive(userApi.usersByActive(viewUsersActive));
    setReloadUsers(false);
  }, [users, viewUsersActive]);

  return (
    <div className="list-users">
      <div className="list-users__header">
        <div className="list-users__header-switch">
          <Switch
            defaultChecked
            onChange={() => setViewUsersActive(!viewUsersActive)}
          />
          <span>
            {viewUsersActive ? 'usuarios activos' : 'usuarios inactivos'}
          </span>
        </div>
        <Button type="primary" onClick={addUserModal}>
          Nuevo usuario
        </Button>
      </div>

      <UsersList
        users={users}
        renderItem={(user) => (
          <User
            user={user}
            editUser={editUser}
            setReloadList={setReloadUsers}
          />
        )}
        editUser={editUser}
        setReloadUsers={setReloadUsers}
      />

      <Modal
        title={properties.title}
        isVisible={properties.isVisible}
        setIsVisible={setIsVisible}
      >
        {properties.content}
      </Modal>
    </div>
  );
}
