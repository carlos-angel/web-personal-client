import React, { useState } from 'react';
import { Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import { menuApi } from 'api/menu';
import MenuWebList from 'components/Admin/MenuWeb/MenuWebList';
import Menu from 'components/Admin/MenuWeb/MenuWebList/MenuWeb';
import Form from 'components/Admin/MenuWeb/Form';
import Modal from 'components/Modal';
import { useData } from 'hooks/useData';
import { useModal } from 'hooks/useModal';

import './MenuWeb.scss';

export default function MenuWeb() {
  const [menu, setReloadMenuWeb] = useData(menuApi.getAll);
  // eslint-disable-next-line no-unused-vars
  const [nextOrder, setNextOrder] = useState(100);

  const {
    properties: { isVisible, title, content },
    setIsVisible,
    setTitle,
    setContent,
  } = useModal();

  const addMenu = () => {
    setTitle('nuevo menu web');
    setIsVisible(true);
    setContent(
      <Form
        isNew
        setIsVisibleModal={setIsVisible}
        setReloadList={setReloadMenuWeb}
        nextOrder={nextOrder}
      />,
    );
  };

  const handleEdit = (editMenu) => {
    setTitle(`Editar el menu ${editMenu.title}`);
    setIsVisible(true);
    setContent(
      <Form
        data={editMenu}
        setIsVisibleModal={setIsVisible}
        setReloadList={setReloadMenuWeb}
        nextOrder={nextOrder}
      />,
    );
  };

  return (
    <div className="menu-web-list">
      <div className="menu-web-list__header">
        <Button type="primary" onClick={addMenu}>
          <PlusOutlined />
          {' '}
          menu web
        </Button>
      </div>
      <MenuWebList
        menu={menu}
        renderItem={(item) => (
          <Menu
            item={item}
            setReloadMenuWeb={setReloadMenuWeb}
            editMenu={handleEdit}
          />
        )}
      />
      <Modal title={title} isVisible={isVisible} setIsVisible={setIsVisible}>
        {content}
      </Modal>
    </div>
  );
}
