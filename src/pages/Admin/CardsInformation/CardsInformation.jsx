import React from 'react';
import { Button } from 'antd';

import { cardsInformationApi } from 'api/card-information';
import Modal from 'components/Modal';
import List from 'components/Admin/CardsInformation/List';
import Item from 'components/Admin/CardsInformation/List/Item';
import Form from 'components/Admin/CardsInformation/Form';
import { useData } from 'hooks/useData';
import { useModal } from 'hooks/useModal';

import './CardsInformation.scss';

export default function CardsInformation() {
  const [cardsInformation, setReloadCards] = useData(
    cardsInformationApi.getCards,
  );

  const {
    properties: { isVisible, title, content },
    setIsVisible,
    setTitle,
    setContent,
  } = useModal();

  const addCard = () => {
    setIsVisible(true);
    setTitle('crear tarjeta');
    setContent(
      <Form
        isNew
        setIsVisibleModal={setIsVisible}
        setReloadCards={setReloadCards}
      />,
    );
  };

  const editCard = (card) => {
    setIsVisible(true);
    setTitle(`Editar la tarjeta: ${card.title}`);
    setContent(
      <Form
        card={card}
        setIsVisibleModal={setIsVisible}
        setReloadCards={setReloadCards}
      />,
    );
  };

  return (
    <div className="cards-information">
      <div className="cards-information__header">
        <Button type="primary" onClick={addCard}>
          Nueva tarjeta
        </Button>
      </div>

      <List
        cards={cardsInformation}
        render={(card) => (
          <Item
            card={card}
            setReloadCards={setReloadCards}
            editCard={editCard}
          />
        )}
      />

      <Modal title={title} isVisible={isVisible} setIsVisible={setIsVisible}>
        {content}
      </Modal>
    </div>
  );
}
