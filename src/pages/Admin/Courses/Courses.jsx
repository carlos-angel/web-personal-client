import React from 'react';
import { Button } from 'antd';

import { coursesApi } from 'api/courses';
import CoursesList from 'components/Admin/Courses/CoursesList';
import Course from 'components/Admin/Courses/CoursesList/Course';
import Form from 'components/Admin/Courses/Form';
import Modal from 'components/Modal';
import { useData } from 'hooks/useData';
import { useModal } from 'hooks/useModal';

import './Courses.scss';

export default function Courses() {
  const [courses, setReloadCourses] = useData(coursesApi.getAll);

  const {
    properties: { isVisible, title, content },
    setIsVisible,
    setTitle,
    setContent,
  } = useModal();

  const handleAddCourse = () => {
    setIsVisible(true);
    setTitle('Crear un curso');
    setContent(
      <Form
        isNew
        setReloadList={setReloadCourses}
        setIsVisibleModal={setIsVisible}
      />,
    );
  };

  const handleEditCourse = (course) => {
    setIsVisible(true);
    setTitle(`Editar el curso ${course.title}`);
    setContent(
      <Form
        data={course}
        setReloadList={setReloadCourses}
        setIsVisibleModal={setIsVisible}
      />,
    );
  };

  return (
    <div className="courses-list">
      <div className="courses-list__header">
        <Button type="primary" onClick={handleAddCourse}>
          Nuevo curso
        </Button>
      </div>
      <div className="courses-list__items">
        {courses.length === 0 && null}
        <CoursesList
          courses={courses}
          isEmpty={<h2> No hay cursos creados </h2>}
          render={(course) => (
            <Course
              course={course}
              handleEditCourse={handleEditCourse}
              setReloadCourses={setReloadCourses}
            />
          )}
        />
      </div>
      <Modal title={title} isVisible={isVisible} setIsVisible={setIsVisible}>
        {content}
      </Modal>
    </div>
  );
}
