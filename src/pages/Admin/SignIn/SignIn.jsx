import React from 'react';
import { Layout, Tabs } from 'antd';
import { Redirect } from 'react-router-dom';
import AdminRegisterForm from 'components/Admin/Auth/RegisterForm';
import AdminLoginForm from 'components/Admin/Auth/LoginForm';
import { getAccessToken } from 'api/auth';
import logo from 'assets/images/png/logo-white.png';
import './SignIn.scss';

export default function SignIn() {
  const { Content } = Layout;
  const { TabPane } = Tabs;

  if (getAccessToken()) {
    return <Redirect to="/admin" />;
  }

  return (
    <Layout className="sign-in">
      <Content className="sign-in__content">
        <h1 className="sign-in__content-logo">
          <img src={logo} alt="Logo CodeTime" />
        </h1>
        <div className="sign-in__content-tabs">
          <Tabs type="card">
            <TabPane tab={<span>Login</span>} key="1">
              <AdminLoginForm />
            </TabPane>
            <TabPane tab={<span>Nuevo usuario</span>} key="2">
              <AdminRegisterForm />
            </TabPane>
          </Tabs>
        </div>
      </Content>
    </Layout>
  );
}
