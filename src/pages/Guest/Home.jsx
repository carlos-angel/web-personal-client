import React from 'react';
import { Helmet } from 'react-helmet';
import MainBanner from 'components/Guest/Home/MainBanner';
import HomeCourses from 'components/Guest/Home/HomeCourses';
import HomeMyCoursesWork from 'components/Guest/Home/HomeMyCoursesWork';
import ReviewsCourses from 'components/Guest/Home/ReviewsCourses';
import { cardsInformationApi } from 'api/card-information';
import { coursesApi } from 'api/courses';
import { apiReview } from 'api/review.api';
import { useData } from 'hooks/useData';

export default function Home() {
  const [cardsCourses] = useData(cardsInformationApi.getCards);
  const [reviews] = useData(apiReview.getReviews);
  const [coursesOnOffer] = useData(coursesApi.getOffers);

  return (
    <>
      <Helmet>
        <title>Carlos Alberto Angel Angel</title>
        <meta
          name="description"
          content="Home | Web sobre programación"
          data-react-helmet="true"
        />
      </Helmet>
      <MainBanner />
      <HomeCourses courses={coursesOnOffer} />
      <HomeMyCoursesWork cardsCourses={cardsCourses} />
      <ReviewsCourses reviews={reviews} />
    </>
  );
}
