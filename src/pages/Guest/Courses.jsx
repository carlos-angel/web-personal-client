import React from 'react';
import { Row, Col, Spin } from 'antd';
import { Helmet } from 'react-helmet';

import PresentationCourse from 'components/Guest/Coruses/PresentationCourse';
import CoursesList from 'components/Guest/Coruses/CoursesList';
import { coursesApi } from 'api/courses';
import { useData } from 'hooks/useData';

export default function Courses() {
  const [courses] = useData(coursesApi.getAll);

  return (
    <>
      <Helmet>
        <title>Cursos | Carlos Alberto Angel Angel</title>
        <meta
          name="description"
          content="Cursos | Web sobre programación de Carlos Alberto Angel Angel"
          data-react-helmet="true"
        />
      </Helmet>
      <Row>
        <Col md={4} />
        <Col md={16}>
          <PresentationCourse />
          {!courses ? (
            <Spin
              tip="cargando cursos"
              style={{ textAlign: 'center', width: '100%', padding: '20px' }}
            />
          ) : (
            <CoursesList courses={courses} />
          )}
        </Col>
        <Col md={4} />
      </Row>
    </>
  );
}
