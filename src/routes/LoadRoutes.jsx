import React from 'react';
import { Route, Switch } from 'react-router-dom';

export default function LoadRoutes({ routes }) {
  return (
    <Switch>
      {routes.map((route, index) => (
        <Route
          // eslint-disable-next-line react/no-array-index-key
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.component}
        />
      ))}
    </Switch>
  );
}
