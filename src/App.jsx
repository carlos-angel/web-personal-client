import { BrowserRouter as Router, Switch } from 'react-router-dom';
import routes from 'config/routes';
import RouterWithSubRoutes from './routes/RouterWithSubRoutes';
import AuthProvider from './providers/AuthProvider';
import './App.scss';

function App() {
  return (
    <AuthProvider>
      <Router>
        <Switch>
          {routes.map((route, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <RouterWithSubRoutes key={index} {...route} />
          ))}
        </Switch>
      </Router>
    </AuthProvider>
  );
}

export default App;
