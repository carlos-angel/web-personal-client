import adminRoutes from 'config/admin.routes';
import guestRoutes from 'config/guest.routes';

const routes = [adminRoutes, guestRoutes];

export default routes;
