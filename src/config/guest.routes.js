import LayoutGuestBasic from 'layouts/Guest/LayoutGuestBasic';
import GuestHome from 'pages/Guest';
import GuestContact from 'pages/Guest/Contact';
import GuestCourses from 'pages/Guest/Courses';
import GuestBlog from 'pages/Guest/Blog';

import Error404 from 'pages/Error/Error404';

const guestRoutes = {
  path: '/',
  component: LayoutGuestBasic,
  exact: false,
  routes: [
    {
      path: '/',
      component: GuestHome,
      exact: true,
    },
    {
      path: '/contact',
      component: GuestContact,
      exact: true,
    },
    {
      path: '/courses',
      component: GuestCourses,
      exact: true,
    },
    {
      path: '/blog',
      component: GuestBlog,
      exact: true,
    },
    {
      path: '/blog/:url',
      component: GuestBlog,
      exact: true,
    },
    {
      component: Error404,
    },
  ],
};

export default guestRoutes;
