import LayoutAdmin from 'layouts/Admin/LayoutAdmin';
import AdminHome from 'pages/Admin';
import AdminSignIn from 'pages/Admin/SignIn';
import AdminUsers from 'pages/Admin/Users/Users';
import MenuWeb from 'pages/Admin/MenuWeb/MenuWeb';
import Courses from 'pages/Admin/Courses/Courses';
import Blog from 'pages/Admin/Blog';
import CardsInformation from 'pages/Admin/CardsInformation';
import Reviews from 'pages/Admin/Reviews/Reviews';

import Error404 from 'pages/Error/Error404';

const adminRoutes = {
  path: '/admin/',
  component: LayoutAdmin,
  exact: false,
  routes: [
    {
      path: '/admin/',
      component: AdminHome,
      exact: true,
    },
    {
      path: '/admin/login',
      component: AdminSignIn,
      exact: true,
    },
    {
      path: '/admin/users',
      component: AdminUsers,
      exact: true,
    },
    {
      path: '/admin/menu',
      component: MenuWeb,
      exact: true,
    },
    {
      path: '/admin/courses',
      component: Courses,
      exact: true,
    },
    {
      path: '/admin/blog',
      component: Blog,
      exact: true,
    },
    {
      path: '/admin/card-information',
      component: CardsInformation,
      exact: true,
    },
    {
      path: '/admin/reviews',
      component: Reviews,
      exact: true,
    },
    {
      component: Error404,
    },
  ],
};

export default adminRoutes;
